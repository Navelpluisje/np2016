---
layout: post
title:  Automated screenshots with python and selenium
date:   2016-05-27 00:33:09 +0100
author: Erwin Goossen
categories: python selenium apps
timerequired: 1:10
---

This happens to be my first working [Python][python] script as far as I know. I did some fiddling before with this language, but not really successfull. Maybe this time it worked out because the script I made can save some serious time.

This script can create screenshots for multiple screensizes and pages at once. I started it because I was working on a [Cordova][cordova] App and needed screenshots for it in the play and appstore. First I started using [Selenium][selenium] with Javascript. Though I like javascript more then Python, it didn't work out for me. For this job Python just did a better job for what I wanted. To get those 2 to work together isn't really hard and there is a lot of [information][information] on the internet.

While creating this script there were some minor pitfalls:

* I had to log in to the app
* What to do with timing
* Is the screensize I give the browser the real screensize?

Logging in itself isn't that hard. Just fill in your credentials and submit. For me the `click()` call didn't work. Still not quite sure why but this can also be done using javascript. This made the login work as a charm.

{% highlight python %}
def login(the_driver):
    # First open our login page
    the_driver.get(config['login']['url'])
    # Don't do a thing before the submit button is available
    # If this one timeouts we quit the the_driver
    try:
        WebDriverWait(the_driver, 15).until(ec.visibility_of_element_located((By.ID, config['login']['submit'])))
    except Exception:
        the_driver.quit()
    finally:
        # Now fill in the foprm and submit the login
        the_driver.find_element_by_id(config['login']['nameid']).send_keys(config['login']['name'], Keys.TAB)
        the_driver.find_element_by_id(config['login']['passwordid']).send_keys(config['login']['password'], Keys.ENTER)
        the_driver.execute_script('document.getElementById("login-button").click()')
{% endhighlight %}

What does this part do? First the driver goes opens the given url. It waits until the submit button is available so we are sure we are able to trigger the button. If it is, we fill in the blanks. After that I used the `execute_script` function to trigger my submit button with javascript. The `config` used is from a settings file. and parameter `the_driver` is the [Selennium driver][driver].

Timing was another issue to tackle down. This was because sometimes the Selenium driver said an element is visible although it wasn't. What can happen if you use animation stuff.

{% highlight python %}
driver.get(shot['url'])
try:
    WebDriverWait(driver, 15).until(ec.visibility_of(driver.find_element_by_class_name(config['global']['visible'])))
except Exception:
    driver.quit()
finally:
    time.sleep(3)
{% endhighlight %}

Just like the example above wait until it says an element is visible. If it is we will wait another 3 seconds (`time.sleep(3)`). You can change this one by need.

The last one happens to be the weirdest one. I'm using the Chromedriver on this one, but the smallest the screen will go is just 400px. There is a workaround by using `mobileEmulation`. This will make your screen smaller, but the actual viewport still is bigger. When creating a screenshot, it will also contain all the areas around. Also if you set the height it will take the toolbar and address bar into it as well which makes it hard to set the actual height.

I fixed this by using [Pillow][pillow] for image cropping and starting with a way bigger viewport then the image needs to be. I started out with creating a viewport of 150% of the emulator size. This way there will never be an issue with the height of the address bar. When taking the screenshot there will be a big gray area on the right and bottom side of the image. The left top corner will be filled with the actual image we want.
Now we use Pillow for cropping the image to the right size.

{% highlight python %}
driver.save_screenshot('tmp.png')
img = Image.open('tmp.png')
img.crop((
    0,
    0,
    device['width'] * config['global']['pixelrate'],
    device['height'] * config['global']['pixelrate'])).save(directory + '/' + shot['name'] + '.png')
{% endhighlight %}
[python]: https://www.python.org/
[selenium]: http://docs.seleniumhq.org/
[driver]: http://docs.seleniumhq.org/docs/03_webdriver.jsp
[cordova]: https://cordova.apache.org/
[information]: https://duckduckgo.com/?q=python+selenium&ia=web
[pillow]: https://pillow.readthedocs.io/en/latest/index.html