/*global window, document */
/*jslint nomen: true*/

(function () {
    'use strict';
    var startScreen,
        startScreenDimensions,
        titleBlockDimensions,
        titleBlock,
        togglePoint,
        toggleTitle,
        // The different states
        fixed = false,
        minimized = false,
        topbar = false;

    /**
     * Handle the 'startscreen' on home while scrolling
     * This is all done by adding or removing classes.
     * The actual transitions are all done with CSS
     * @private
     */
    function _homeScroll() {
        titleBlockDimensions = titleBlock.getBoundingClientRect();
        startScreenDimensions = startScreen.getBoundingClientRect();
        if (!fixed && titleBlockDimensions.top <= 0) {
            fixed = true;
            togglePoint = window.scrollY;
            titleBlock.classList.add('fixed');
        }

        if (fixed) {
            if (window.scrollY < togglePoint) {
                fixed = false;
                titleBlock.classList.remove('fixed');
            }

            if (minimized) {
                if (!topbar && window.scrollY > startScreenDimensions.height - titleBlockDimensions.height && window.scrollY >= titleBlockDimensions.height) {
                    topbar = true;
                    titleBlock.classList.add('topbar');
                }

                if (topbar && window.scrollY < startScreenDimensions.height - titleBlockDimensions.height) {
                    topbar = false;
                    titleBlock.classList.remove('topbar');
                }

                if (window.scrollY < toggleTitle) {
                    minimized = false;
                    titleBlock.classList.remove('shrink');
                }
            } else {

                if (startScreenDimensions.top + startScreenDimensions.height <= titleBlockDimensions.height) {
                    toggleTitle = window.scrollY;
                    minimized = true;
                    titleBlock.classList.add('shrink');
                }
            }
        }
    }

    /**
     * Toggle the menu
     * @private
     */
    function _toggleMenu() {
        document.getElementById('menu-site').classList.toggle('show');
    }

    /**
     * Add the scrollevent to initialize the scrolleffects on the homepage
     * @private
     */
    function _setHomeBindings() {
        document.addEventListener('scroll', _homeScroll);
    }

    /**
     * Add eventhandling for all pages, except home
     * @private
     */
    function _setEventBindings() {
        document.getElementById('menu-toggle').addEventListener('click', _toggleMenu);
    }

    /**
     * Initialising the site/page. Set all eventbindings and do some more stuff on home
     * @private
     */
    function _init() {
        if (document.querySelector('.site-start')) {
            startScreen = document.querySelector('.site-start');
            titleBlock = document.querySelector('.site-start article');
            document.querySelector('h1').classList.add('show');
            document.querySelector('.site-start section').classList.add('show');
            _setHomeBindings();
        }
        _setEventBindings();
    }

    /**
     * Call _init to initialize all our javascript
     */
    _init();
}());