/*jslint nomen: true */
/*global window, document */

var jlAnimation = (function () {
    'use strict';
    var init = true,
        elements;

    /**
     * We handle the actual stuff over here.
     * We/'re looping through all the elements and add the show class if the
     * element is in our viewport. Th einit class is set if the element is
     * within the viewport on loading the page.
     *
     * @return {void}
     */
    function _handleScroll() {
        var theHeight = window.innerHeight,
            i = 0,
            obj,
            startAnimation,
            offset;

        for (i; i < elements.length; i += 1) {
            obj = elements[i];
            startAnimation = obj.getBoundingClientRect().top;
            offset = +obj.dataset.jlAnimateOffset || 0;

            if ((startAnimation + offset) < theHeight) {
                if (init) {
                    obj.classList.add('init');
                }
                obj.classList.add('animated');
            }
        }

        // Set init to false after our first run
        init = false;
    }

    /**
     * Set the eventbinding for scrolling
     */
    function _setEventBindings() {
        window.addEventListener('scroll', _handleScroll);
    }

    /**
     * Do some initialization. Create an array of all the animatable elements,
     * run the init scroll to show the vissible items
     * and set the event bindings for scrolling
     *
     * @return {void}
     */
    function _init() {
        elements = document.querySelectorAll('[data-jl-animate]');
        _handleScroll(true);
        _setEventBindings();
    }

    _init();
}());
