---
layout: page
title: About
permalink: /about/
image: icons/icon-head.svg
---

My name's Erwin Goossen and am from the middle of the Netherlands near Utrecht. I was born in the mid seventies. To be more precise, the year Sesame Street was on dutch tele for the first time.

I work as a webdeveloper at Justlease.nl, the little daughter of Terberg Leasing, where I'm resposible for the private leasing sites (<a target="_blank" href="http://leasen.nl">leasen.nl</a>).

In my sparetime I'm working on some smaller projects, my own site, some other ones and trying to absorb all the information my tiny brains can hold. Beside that I play the drums in a post-rockish band named <a target="_blank" href="http://nwcc.nl">Nope We Can't Cope</a> (nwcc), like to drink belgian beers and visit as many concerts as possible.

With this site I want to show you me as a developer, not a musician.
