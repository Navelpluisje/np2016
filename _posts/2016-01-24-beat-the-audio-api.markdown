---
layout: post
title:  Beat the javascript audio API
date:   2016-01-24 00:33:09 +0100
author: Erwin Goossen
categories: javascript audio
timerequired: 00:54
---
A few years ago I build a small javascript drum and bass machine. This was mainly build upon the `<audio>`-tag. While attending a MeetUp in Utrecht, [Peter Peerdeman][peerdeman] told in his pitch about [DDP][ddp]. After the pitch I was thinking about some possibilities where and how I could use this protocol. After about 5 minutes, pretty quick I think, my good 'ol Beatmachine came to my mind. I thought about how cool it would be to make music with multiple persons at once. 

Because of the speed of DDP and the easy implementation of the protocol I started of thinking how to get this all working. The solution would be a fully rewritten Beatmachine which uses the [Web Audio API][audio-api]. This post is the first in a serie of 'I'm not quite sure how many' posts about the Beatmachine, the used techniques and probably more.

The beat machine is build around a couple of modules. The next couple of posts will explain more about the:

* **sounds-module**: The main part which contains the buffering, manipulating and playing of the drum samples
* **channel-module**: Controlling the sounds
* **sequencer**: The part where the beats are created
* **bass synth**: A monophonic, simple bass generator
* **connectivity**: The DDP connection
* and maybe more

The first part of the serie will be published in about one or two weeks. Hope to see you then again.


[peerdeman]: http://www.peterpeerdeman.nl/
[ddp]: https://www.meteor.com/ddp
[audio-api]: https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
