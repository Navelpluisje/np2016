---
layout: post
title:  "Fix cache problem Jasmine tests with Gulp"
date:   2014-06-03 23:33:09 +0100
author: Erwin Goossen
categories: javascript gulp jasmine
---
For my normal job I use Codekit. But after reading some positive things about [Gulp][gulp] I got curious about the things it can and can't do for me. So I decided to try it, and made me a small test-project to figure some things out. I've added some scss files to it (to compile), some javascript files (to concat and uglify) and some jasmine test scripts (for automatic testing) as well.

Before this I gave [Grunt][grunt] a small try, but couldn't get be friends with it's syntax. Instead of the JSON style Grunt uses, Gulp uses 'plain' javascript for this purpose. This file is splitted in several tasks which makes it pretty easy, for me, to understand. In this post I won't tire you with all the ins and outs of Gulp, 'cause there are a lot of good articles about it, so why would I bother. Instead I will write about extending the [Gulp-Jasmine][gulp-jasmine] plugin. This because I ran into some problems with it.

While using this plugin I ran into the problem that while running the Gulpfile a second time after changing the javascript file, the result of the test remains the same as with the previous version of your javascript file. The problem only occurs if you triggered the test with a watch function. After taking a closer look to the code I found out the problem was the caching meganism of Nodes require.

When you include a file using the require function in Node, the file will get cached so they don't have to be loaded a second time when you want to use this specific file again. Most of the time this functionality is pretty handy and there are not many reason to change this functionality. One of the reasons I found was the use of Gulp-Jasmine. When creating a test-script to use with the plugin, you use require to include the file to run the tests on. And here comes the problem. If you change your scripts content and save it, the watch function of Gulp comes in. Within the watch you can call you're script and voilla, there you go. At least I thought I was going.

Within the plugin there is only one file which gets deleted from the require-cache, the test itself. Pretty obvious to do, because you can now modify your test script file and run it again. The problem is that the actual files to test aren't deleted from the require-cache. This is what I wanted to change (I found some threads mon stack overflow also on this point, so I'm not the only one I think) and modified the plugin a bit. The only part I've added is the code below:

{% highlight javascript %}
/**
 * Get the cacheobject of the specs.js file,
 * get it's children and delete the childrens cache
 */
var files = require.cache[require.resolve(path.resolve(file.path))];
if (typeof files !== 'undefined') {
    for (var i in files.children) {
        delete require.cache[files.children[i].id];
    }
}
{% endhighlight %}

Well what does this little chunk of code do? That's pretty easy. He're a little desription of what's happening:

First we're getting the require-cache of the testscript. The require-cache system of Node exists as a JSON file. Each filename is it's own key, so the cache of the testscript is easy to determine.
Every file this testscript has included has been added to the require-cache as well. These two files are linked to each other in the cache of the testscript and are stored under the 'children'. Each child has an id which is the same as the files require-cache id of the actual file.
The only thing we have to do now is loop through the children and delete the require-cache items with the corresponding id. That's all. Now every time the Gulp-Jasmine plugin is called, the complete cache corresponding to the test is deleted and the result will be accurate.
I hope this will help you out with javascript testing with Jasmine and Gulp.


**note**: I've added a pullrequest for this issue on [https://github.com/sindresorhus/gulp-jasmine/pull/21][pull]

**UPDATE**: The pullrequest has been added to the code

[gulp]: http://gulpjs.com
[grunt]: http://gruntjs.com
[gulp-jasmine]: https://github.com/sindresorhus/gulp-jasmine
[pull]: https://github.com/sindresorhus/gulp-jasmine/pull/21
