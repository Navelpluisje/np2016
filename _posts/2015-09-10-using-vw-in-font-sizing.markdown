---
layout: post
title:  "Using vw in font-sizing"
date:   2015-09-10 23:33:09 +0100
author: Erwin Goossen
categories: css
---
Just had a great workshop today about Responsive Design given by [Frances de Waal]. She teached and told us aspecialy about the basics, which is pretty refreshing after you're used in using framworks for most of the time. During the workshop there's a part about responsive typography and using the vw unit for heading sizing. We were wondering if we could use this in a way to not have to use any media-query.

Main pitfall using the vw unit is the fact that the fonts will get really small when the width of the viewport decreases. The example below shows you what happens when using only the vw for your headings.

<p class="codepen" data-default-tab="css" data-height="268" data-slug-hash="jbbyZw" data-theme-id="4342" data-user="navelpluisje">See the Pen <a href="http://codepen.io/navelpluisje/pen/jbbyZw/">Heading with VW</a> by Navelpluisje (<a href="http://codepen.io/navelpluisje">@navelpluisje</a>) on <a href="http://codepen.io">CodePen</a>.</p>

When scaling the screen down you see the font getting really small in the end. After fiddling around with some of the units we have nowadays I ended up using the next pen:

<p class="codepen" data-default-tab="css" data-height="268" data-slug-hash="wKKgEo" data-theme-id="4342" data-user="navelpluisje">See the Pen <a href="http://codepen.io/navelpluisje/pen/wKKgEo/">Heading with vw and vmax</a> by Navelpluisje (<a href="http://codepen.io/navelpluisje">@navelpluisje</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//assets.codepen.io/assets/embed/ei.js"></script>

Because of the use of the vmax we decrease the amount of scaling. This happens because not the full font-size scales while changin the viewports width. Another side effect of this combination is that even if the viewport gets really small we still depend partly on the vmax unit. This will prevent the font from getting unreadable.

I hope I gave you some hands in using scalable heading without media queries

[Frances de Waal]: http://waalweb.nl
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
