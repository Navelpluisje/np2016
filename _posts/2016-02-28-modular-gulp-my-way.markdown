---
layout: post
title:  Modular Gulp, my way
date:   2016-02-28 00:33:09 +0100
author: Erwin Goossen
categories: javascript gulp
timerequired: 1:10
---

A lot of people currently using task managers like [Gulp][gulp]. Most of them are confronted with the same problem of a huge `gulpfile.js`. I had the same problem and started searching for a solution. After a not so long search I found a post by [Eric Brehault][modular]. His solution for me still had one downside: re-usability of the same task.

For some projects I work on there are many javascript files. Some of them needs to be concatenate, others don't but they all need to get uglified. The way described by Eric you still need a file per task. Even if the task could get reused for other tasks. 

#### The solution

For me the solution to this was generating tasks buy the content of the configuration file. I will give a small and simplified example of how I did this.

First create the configuration file. Within this file you need to group all the files for the same tasks with the task name as the key.

{% highlight javascript %}
javascript: {
	js_1: {
		source: 'abc/def/**/*js',
		destination: '/xyz/',
		name: 'task-js-1'
	},
	js_2: {
		source: 'abc/def/**/*js',
		destination: '/xyz/',
		name: 'task-js-1'
	},
	js_3: {
		source: 'abc/def/**/*js',
		destination: '/xyz/',
	},
}
{% endhighlight %}

Within the taskfile created we can get all the keys of the javascript and loop through them to generate the different tasks. This can look like:

{% highlight javascript %}
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    config = require('./configuration').javascript,
    tasks = Object.keys(config);

function addTask(element) {
    gulp.task('scripts', function () {
        return gulp.src(config[element].source)
            .pipe(uglify())
            .pipe(rename(config[element].name))
            .pipe( gulp.dest(config[element].destination))
        }
    );	
}

tasks.forEach(addTask);
{% endhighlight %}

Now all the tasks for our javascript are created. For me this helped me reducing the number of task-files. This makes it easier to maintain for me and my co-workers.
I hope this will help you to in creating a better work environment.

[gulp]: http://gulpjs.com/
[modular]: http://makina-corpus.com/blog/metier/2015/make-your-gulp-modular
