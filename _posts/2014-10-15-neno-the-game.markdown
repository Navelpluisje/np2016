---
layout: post
title:  "Neno: the game"
date:   2014-10-15 23:33:09 +0100
author: Erwin Goossen
categories: javascript craftyjs
---
I've released a small game named Neno. It can be found on [http://neno.navelpluisje.nl][neno]. It's a small 2 level mario-like game. Is it special? Not really, but it made me take a dive into [Craftyjs][crafty]. An easy to use, pretty versatile javascript library that let you make some nice 2D games.

I started creating the game after I read a tutotial about it which I didn't like. The game created within that tutorial wasn't a real game to me. So I started creating my own game. This way I had to take a deep dive into Crafty.

[http://buildnewgames.com/introduction-to-crafty/][buldnewgames] was the base for my game (this is not the tutorial mentioned before). I gave it a bit of gravity, created some nicer graphics, a bit more gameplay and there it is. I will post some more about some difficulties I ran into soon. Also the code will be available soon. Have a nice play! 

[neno]: http://neno.navelpluisje.nl 
[crafty]: http://craftyjs.com/
[buildnewgames]: http://buildnewgames.com/introduction-to-crafty/