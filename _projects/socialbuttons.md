---
layout: project
title:  "SocialButtons"
categories: php
repository: https://bitbucket.org/Navelpluisje/socialbuttons
image: /images/bolt-extension.png
---

The "Social Buttons" is a small extension to insert 'Social' buttons in your templates.