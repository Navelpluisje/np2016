---
layout: project
title:  "Beatmachine"
categories: javascript, html
repository: https://bitbucket.org/Navelpluisje/beatmachine
visit: http://beatmachine.navelpluisje.nl
image: https://bitbucket.org/Navelpluisje/beatmachine/avatar/128/?ts=1449000890
---

An easy to use simple drummachine.

It has been written in pure javascript and has just one dependency, which I also made ([npKnob](https://bitbucket.org/Navelpluisje/npknob)) for the rotary knobs.
After cloning just `after cloning` below.

#### Compatibility ####
This nice piece of Dutch engineering works with the following browsers:
Chrome, Firefox (With serverconnection from version 44, currently nightly), Safari, iOS9 (Yep, works on da iPad).

---
[More](https://bitbucket.org/Navelpluisje/beatmachine/wiki/Home.md)