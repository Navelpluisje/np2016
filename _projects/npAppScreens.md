---
layout: project
title:  "npAppScreens"
categories: python, selenium
repository: https://bitbucket.org/Navelpluisje/npappscreens
image: /images/npAppScreen-small.png
---

Small python script for creating screenshots with multiple dimensions of different pages in once.

It has been written in python and has the following dependencies:

* selenium `pip install selenium`
* Pillow, for image manipulation `pip install Pillow`
* PyYAML, for handling the config file `pip install PyYAML`

