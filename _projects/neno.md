---
layout: project
title:  "Neno"
categories: javascript
repository: https://bitbucket.org/Navelpluisje/neno
visit: http://neno.navelpluisje.nl
image: /images/neno-icon.jpg
---

Just a small 2D game in javascript using the crafty framework. 