---
layout: project
title:  "npKnob"
categories: javascript
repository: https://bitbucket.org/Navelpluisje/npknob
image: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2015/Oct/06/633244558-4-npknob-logo_avatar.png
---

A small pure javascript library for creating rotating knobs. It was created while I had the idea to rebuild my BeatMachine.

#### Install 
Run `npm install np-knob`, or download your version from [BitBucket][download]

#### Usage

* 
Create an element with an id. Then add the next code:
{% highlight ruby %}
var knob1 = new NpKnob('your-id', options);
{% endhighlight %}

[download]: https://bitbucket.org/Navelpluisje/npknob/downloads